@php

$colname = $column['name'];

$field = $entry->$colname;
//dump($entry);
if (isset($entry)) {


if($field === null ){
$field = [];

}


$default['height'] = '100';
$default['width'] =  '2';
$default['textMargin'] = '0';
$default['margin'] =  '0';
$default['format'] = 'EAN13';
$default['fontSize'] =  '11';
$default['lineColor'] = '#000000';
$default['background'] =  '#FFFFFF';
$default['font'] =  'Arial';
$default['fontOptions'] =  json_encode([], true);
$default['textAlign'] = 'center';


$field = array_merge($default,$field);


$barcode_field = $column['barcode_field'] ?? 'barcode_data';
$barcodeValue = $entry->$barcode_field;
} else {
$barcodeValue = '';
}

@endphp


<div data-handler="barcode" data-value="{{$barcodeValue}}" data-config="{{json_encode($field,true)}}">

    <div class="barcode-container">
        <canvas id="barcode"></canvas>
    </div>
</div>

<script src="{{ asset('assets/packages/jsbarcode/dist/JsBarcode.all.min.js') }}"></script>
<script>

    function getBarCodeConfig(value) {
        let height = value['height']
        let width = value['width']
        let margin = value['margin']
        let textMargin = value['textMargin']
        let format = value['format']
        let fontSize = value['fontSize']
        let lineColor = value['lineColor']
        let background = value['background']
        let font = value['font']
        let fontOptions = value['fontOptions']
        let textAlign = value['textAlign']

        return {
            "format": format,
            "background": background,
            "lineColor": lineColor,
            "fontSize": parseInt(fontSize),
            "height": parseInt(height),
            "width": parseInt(width),
            "margin": parseInt(margin),
            "textMargin": parseInt(textMargin),
            "displayValue": true,
            "font": font,
            "fontOptions": [],
            "textAlign": textAlign,

        };
    }
    document.addEventListener("DOMContentLoaded", function () {
        let column = document.querySelector("div[data-handler='barcode']");

        let value = column.dataset.value;

        let config = getBarCodeConfig(JSON.parse(column.dataset.config));
        JsBarcode('#barcode',
            value, {
            ...config,
            "valid": function (valid) {
                if (valid) {
                    $("#barcode").show();
                    $("#invalid").hide();
                } else {
                    $("#barcode").hide();
                    $("#invalid").show();
                }
            }
        }
        );
    })
</script>