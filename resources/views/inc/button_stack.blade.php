@if (CRUD::getOperationSetting('dropdownActions') && $stack =='line')
    <button id="btnGroupDrop1" type="button" class="btn btn-sm btn-secondary dropdown-toggle" data-toggle="dropdown"
        aria-haspopup="true" aria-expanded="false">
        Actions
    </button>
    <div class="dropdown-menu" aria-labelledby="btnGroupDrop1">

        @if ($crud->buttons()->where('stack', $stack)->count())
            @foreach ($crud->buttons()->where('stack', $stack) as $button)
                {!! $button->getHtml($entry ?? null) !!}
            @endforeach
        @endif
    </div>
@else
    @if ($crud->buttons()->where('stack', $stack)->count())
        @foreach ($crud->buttons()->where('stack', $stack) as $button)
            {!! $button->getHtml($entry ?? null) !!}
        @endforeach
    @endif
@endif
