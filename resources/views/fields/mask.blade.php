<!-- field_type_name -->
@php
    $field['wrapper']['data-init-function'] = $field['wrapper']['data-init-function'] ?? 'initMask';
    $mask_settings = $field['mask'];

@endphp
@include('crud::fields.inc.wrapper_start')
<label>{!! $field['label'] !!}</label>
<input unmask="typed" type="text" name="{{ $field['name'] }}"
    value="{{ old($field['name'])? old($field['name']): (isset($field['value'])? $field['value']: (isset($field['default'])? $field['default']: '')) }}"
    @include('crud::fields.inc.attributes')>

{{-- HINT --}}
@if (isset($field['hint']))
    <p class="help-block">{!! $field['hint'] !!}</p>
@endif
@include('crud::fields.inc.wrapper_end')

@if ($crud->fieldTypeNotLoaded($field))
    @php
        $crud->markFieldTypeAsLoaded($field);
    @endphp

    {{-- FIELD EXTRA CSS --}}
    {{-- push things in the after_styles section --}}
    @push('crud_fields_styles')
        <!-- no styles -->
    @endpush

    {{-- FIELD EXTRA JS --}}
    {{-- push things in the after_scripts section --}}
    @push('crud_fields_scripts')
        <!-- no scripts -->
        <script src="/packages/imask.js"></script>

        <script>
            function initMask(element){
              let settings = {!!json()->encode($mask_settings)!!}
              console.log(settings);
              IMask(element.find('input')[0],settings);
            }

        </script>
    @endpush
@endif
