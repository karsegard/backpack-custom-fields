@php
$field['prefix'] = $field['prefix'] ?? '';
$field['disk'] = $field['disk'] ?? null;
$value = old(square_brackets_to_dots($field['name'])) ?? ($field['value'] ?? ($field['default'] ?? []));

$field['wrapper'] = $field['wrapper'] ?? ($field['wrapperAttributes'] ?? []);
$field['wrapper']['class'] = $field['wrapper']['class'] ?? 'form-group col-sm-12';
$field['wrapper']['class'] = $field['wrapper']['class'] . ' jsbarcode';

$field['wrapper']['data-field-name'] = $field['wrapper']['data-field-name'] ?? $field['name'];
$field['wrapper']['data-init-function'] = $field['wrapper']['data-init-function'] ?? 'bpFieldInitJSBarcodeElement';

$default['height'] =  '100';
$default['width'] =  '2';
$default['textMargin'] =  '0';
$default['margin'] =  '0';
$default['format'] =  'EAN13';
$default['fontSize'] = '11';
$default['lineColor'] =  '#000000';
$default['background'] = '#FFFFFF';
$default['font'] = 'Arial';
$default['fontOptions'] =  json_encode([], true);
$default['textAlign'] =  'center';

$value = array_merge($default,$value);

$value = json_encode($value, true);

//dump($entry);
if (isset($entry)) {
    $barcode_field = $field['barcode_field'] ?? 'barcode_data';
    $barcodeValue = $entry->$barcode_field;
} else {
    $barcodeValue = '';
}

$field['wrapper']['data-barcodevalue'] = $barcodeValue;
//$barcodeValue =
@endphp

@include('crud::fields.inc.wrapper_start')
<div>
    <label>{!! $field['label'] !!}</label>
    @include('crud::fields.inc.translatable_icon')
</div>
{{-- Wrap the image or canvas element with a block element (container) --}}
<div class="row">
    <input type="hidden" data-handle="hiddenContent" name="{{ $field['name'] }}" value="{{ $value }}" />
    <div class="barcode-container">
        <canvas id="barcode"></canvas>
        <span id="invalid">Not valid data for this barcode type!</span>
    </div>
</div>
{{-- HINT --}}
@if (isset($field['hint']))
    <p class="help-block">{!! $field['hint'] !!}</p>
@endif
@include('crud::fields.inc.wrapper_end')


{{-- ########################################## --}}
{{-- Extra CSS and JS for this particular field --}}
{{-- If a field type is shown multiple times on a form, the CSS and JS will only be loaded once --}}
@if ($crud->fieldTypeNotLoaded($field))
    @php
        $crud->markFieldTypeAsLoaded($field);
    @endphp

    {{-- FIELD CSS - will be loaded in the after_styles section --}}
    @push('crud_fields_styles')
        <style>

        </style>
    @endpush

    {{-- FIELD JS - will be loaded in the after_scripts section --}}
    @push('crud_fields_scripts')

        <script src="{{ asset('assets/packages/jsbarcode/dist/JsBarcode.all.min.js') }}"></script>
        <script>
            let barcodeConfig = {};

            function getBarCodeConfig(value) {
                let height = value['height']
                let width = value['width']
                let margin = value['margin']
                let textMargin = value['textMargin']
                let format = value['format']
                let fontSize = value['fontSize']
                let lineColor = value['lineColor']
                let background = value['background']
                let font = value['font']
                let fontOptions = value['fontOptions']
                let textAlign = value['textAlign']

                return {
                    "format": format,
                    "background": background,
                    "lineColor": lineColor,
                    "fontSize": parseInt(fontSize),
                    "height": parseInt(height),
                    "width": parseInt(width),
                    "margin": parseInt(margin),
                    "textMargin": parseInt(textMargin),
                    "displayValue": true,
                    "font": font,
                    "fontOptions": [],
                    "textAlign": textAlign,

                };
            }

            function makeBarCode(value,config) {
                $("#barcode").JsBarcode(
                    value, {
                        ...config,
                        "valid": function(valid) {
                            if (valid) {
                                $("#barcode").show();
                                $("#invalid").hide();
                            } else {
                                $("#barcode").hide();
                                $("#invalid").show();
                            }
                        }
                    }
                );
            }

            function bpFieldInitJSBarcodeElement(element) {
                let hiddenContent = element.find("[data-handle=hiddenContent]");
                barcodeConfig = getBarCodeConfig(JSON.parse(hiddenContent.val()));
                let barcodevalue = element.data('barcodevalue')
                makeBarCode(barcodevalue, barcodeConfig);
            }
        </script>


    @endpush
@endif
{{-- End of Extra CSS and JS --}}
{{-- ########################################## --}}
