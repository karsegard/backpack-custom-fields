@php
use Balping\JsonRaw\Encoder;
$value = old(square_brackets_to_dots($field['name'])) ?? ($field['value'] ?? ($field['default'] ?? ''));
$field['wrapper']['data-field-name'] = $field['wrapper']['data-field-name'] ?? $field['name'];
$field['wrapper']['data-init-function'] = $field['wrapper']['data-init-function'] ?? 'initEditorJS';
$field['wrapper']['data-elfinder-trigger-url'] = url(config('elfinder.route.prefix') . '/popup');

$field['hyperlink_options'] = array_replace_recursive([], $field['hyperlink_options'] ?? []);

$field['wrapper']['data-hyperlink_options'] = json_encode($field['hyperlink_options'] ?? []);

$tools = config('kda.backpack.custom_fields.ejs.tools') ?? [];
$toolbar = config('kda.backpack.custom_fields.ejs.toolbar') ?? ['bold', 'italic', 'hyperlink'];

$default_toolbar = config('kda.backpack.custom_fields.ejs.default_toolbar') ?? ['bold', 'italic'];

$field['ejs_tools'] = $field['ejs_tools'] ?? [
    'header' => [],
    'hyperlink' => [
        'availableTargets' => [
            ['_self' => 'Même Fenêtre','_blank' => 'Nouvelle Fenêtre',]
              
            
        ],
    ],
];

$tools = array_intersect_key($tools, $field['ejs_tools']);

foreach ($tools as $key => $tool) {
    $conf = $field['ejs_tools'][$key] ?? [];
    $configConf = $tools[$key]['config'] ?? [];
    
    foreach ($configConf as $key2 => $tool2) {
        if (isset($conf[$key2])) {
            $tools[$key]['config'][$key2] = $conf[$key2];
        }
    }
}


$toolbar = array_keys(array_intersect_key(array_flip($toolbar), array_merge($field['ejs_tools'], array_flip($default_toolbar))));
$field['tools'] = Encoder::encode($tools);
$field['ejs_toolbar'] = Encoder::encode($toolbar);

if (is_array($value)) {
    $value = json_encode($value);
}

@endphp
@include('crud::fields.inc.wrapper_start')
    <label>{!! $field['label'] !!}</label>
    @include('crud::fields.inc.translatable_icon')
{{-- Wrap the image or canvas element with a block element (container) --}}
<div class="gka-backpack-editorjs-wrapper">
    <div data-handle="editorjs"></div>
</div>
<input data-handle="hiddenContent" class="array-json" type="hidden" name="{{ $field['name'] }}"
    value="{{ $value }}" />

    <br/>
    <br/>
    <br/>
    <br/>
{{-- HINT --}}
@if (isset($field['hint']))
    <p class="help-block">{!! $field['hint'] !!}</p>
@endif

@include('crud::fields.inc.wrapper_end')


{{-- ########################################## --}}
{{-- Extra CSS and JS for this particular field --}}
{{-- If a field type is shown multiple times on a form, the CSS and JS will only be loaded once --}}
@if ($crud->fieldTypeNotLoaded($field))
    @php
        $crud->markFieldTypeAsLoaded($field);
    @endphp

    {{-- FIELD CSS - will be loaded in the after_styles section --}}
    @push('crud_fields_styles')
        <link href="{{ asset('packages/cropperjs/dist/cropper.min.css') }}" rel="stylesheet" type="text/css" />
        <link href="{{ asset('packages/jquery-colorbox/example2/colorbox.css') }}" rel="stylesheet" type="text/css" />

        <style>
            #cboxContent,
            #cboxLoadedContent,
            .cboxIframe {
                background: transparent;
            }

        </style>
    @endpush

    {{-- FIELD JS - will be loaded in the after_scripts section --}}
    @push('crud_fields_scripts')
        <script src="{{ asset('packages/editorjs/editor.js') }}"></script>
        <script src="{{ asset('packages/editorjs/tools/header.js') }}"></script>
        <script src="{{ asset('packages/editorjs/tools/list.js') }}"></script>
        <script src="{{ asset('packages/editorjs/tools/table2.js') }}"></script>
        <script src="{{ asset('packages/editorjs/tools/embed.js') }}"></script>
        <script src="{{ asset('packages/editorjs/tools/simple-image.js') }}"></script>
        <script src="{{ asset('packages/editorjs/tools/hyperlink.js') }}"></script>
        <script src="{{ asset('packages/jquery-ui-dist/jquery-ui.min.js') }}"></script>
        <script src="{{ asset('packages/jquery-colorbox/jquery.colorbox-min.js') }}"></script>

        <script>
            // this global variable is used to remember what input to update with the file path
            // because elfinder is actually loaded in an iframe by colorbox
            var elfinderTarget = false;


            var editorJSHyperlinkCallback = false;




            // function to update the file selected by elfinder
            function editorJSprocessSelectedFile(filePath, requestingField) {
                //  if (editorJSHyperlinkCallback) {
                elfinderTarget.val(filePath.replace(/\\/g, "/"));
                elfinderTarget = false;
                editorJSHyperlinkCallback(filePath);
                editorJSHyperlinkCallback = false;
                // }
            }



            function initEditorJS(element) {
                var triggerUrl = element.data('elfinder-trigger-url')
                var name = "{{ $field['name'] }}";
                var hyperlink_options = element.data('hyperlink_options');
                console.log(hyperlink_options);
                var browseCallback = function(callback) {

                    elfinderTarget = element;

                    // trigger the reveal modal with elfinder inside
                    $.colorbox({
                        href: triggerUrl + '/' + name + '?callback=editorJSprocessSelectedFile',
                        fastIframe: true,
                        iframe: true,
                        width: '80%',
                        height: '80%'
                    });

                    editorJSHyperlinkCallback = callback;
                    // callback('file.txt');
                };

                var $hiddenContent = element.find("[data-handle=hiddenContent]");
                var $editor = element.find("[data-handle=editorjs]")[0];

                console.log(triggerUrl, name);
                console.log('initial data', $hiddenContent.val());
                var data = {};

                try {
                    data = JSON.parse($hiddenContent.val()) || {};
                } catch (e) {

                }
                var editor = new EditorJS({
                    holder: $editor,
                    inlineToolbar: {!! $field['ejs_toolbar'] !!},
                    minHeight: 50,
                    tools: {!! $field['tools'] !!},
                    onChange: (e) => {
                        editor.save().then((outputData) => {
                            $hiddenContent.val(JSON.stringify(outputData));
                        });

                    },
                    data
                });
            }
        </script>


    @endpush
@endif
{{-- End of Extra CSS and JS --}}
{{-- ########################################## --}}
