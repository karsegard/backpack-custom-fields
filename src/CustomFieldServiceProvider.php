<?php

namespace KDA\Backpack\Field;

use KDA\Laravel\PackageServiceProvider;

class CustomFieldServiceProvider extends PackageServiceProvider
{
   use \KDA\Laravel\Traits\HasViews;
   use \KDA\Laravel\Traits\HasPublicAssets;
   use \KDA\Laravel\Traits\HasConfig;

   protected $registerViews = 'kda-backpack-custom-fields';
   protected $publishViewsTo = 'vendor/backpack/base';
   protected function packageBaseDir () {
       return dirname(__DIR__,1);
   }

   protected $configs= [
    'kda/backpack/custom_fields.php'  =>'kda.backpack.custom_fields'
   ];
   public function boot(){
    parent::boot();
   /* $this->loadViewsFrom( $this->path($this->viewsDir,'/vendor/backpack/crud'), 'kda-backpack');*/
   }

}