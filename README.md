
## Dev or upgrade js assets


    yarn 

    yarn mix



    sail artisan vendor:publish --provider="KDA\Backpack\Field\CustomFieldServiceProvider" --tag="public" --force


## Croppable Image Field

### install


    sail composer require intervention/image

and follow https://image.intervention.io/v2/introduction/installation#integration-in-laravel



    CRUD::addField([
            'label' => "Couverture",
            'name' => "cover",
            'type' => 'imagecrop',
            'tab'=> 'Media',

            // imagecrop
            'original_field'=> 'original',
            'metadata_field'=> 'crop',
            'crop' => true, // set to true to allow cropping, false to disable
            'aspect_ratio' => 1, // omit or set to 0 to allow any aspect ratio
            //'crop' => true, // set to true to allow cropping, false to disable
            //'aspect_ratio' => 1, // omit or set to 0 to allow any aspect ratio
            // 'disk'      => 's3_bucket', // in case you need to show images from a different disk
            // 'prefix'    => 'uploads/images/profile_pictures/' // in case your db value is only the file name (no path), you can use this to prepend your path to the image src (in HTML), before it's shown to the user;
        ]);

### Model trait

cast your attribute as array and use the Croppable trait


    class Book extends Model {
        use \KDA\Backpack\Field\Model\Croppable;


        protected $casts = [
            'cover'=> 'array',
            ...
        ]

        protected $croppable = [
        
            'cover'=>[ // field name
                'path'=> 'press' // path in storage
            ]
        ];

