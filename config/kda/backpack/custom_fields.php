<?php
use Balping\JsonRaw\Raw;
return [
    'ejs' => [
        'tools'=>[
            "header"=>[
                "class"=> new Raw("Header"),
                "config"=> [
                    "levels"=> [2, 3]
                ]
            ],
            "list"=>[
                "class"=> new Raw("List"),
                "inlineToolbar"=> true,

            ],
            "table"=>[
                "class"=> new Raw("Table"),
                "inlineToolbar"=> true
            ],
            "hyperlink"=>[
                "class"=> new Raw("Hyperlink"),
                "config"=> [
                    "shortcut"=>  new Raw("'CMD+L'"),

                    "availableTargets"=> [
                        ["self"=> "'Même Fenêtre'"],
                        ["blank"=> "'Nouvelle Fenêtre'"]
                    ],
                    "availableRels"=> [],
                    "validate"=> false,
                    "shouldAppendProtocol"=> false,
                    "shouldMakeLinkAbsolute"=> true,
                    
                    "browseCallback"=> new Raw("browseCallback")
                ],
            ],
            "embed"=>[
                "class"=> new Raw("Embed"),
                "config"=> [
                    "services"=> [
                        "youtube"=> true,
                        "coub"=> true,
                        "vimeo"=> true,
                ]
                ]
            ],
            "image"=>[
                "class"=> new Raw("SimpleImage"),
                "inlineToolbar"=>true,
                "config"=>[
                    "browseCallback"=> new Raw("browseCallback"),
                    "shouldAppendProtocol"=> false,
                    "shouldMakeLinkAbsolute"=> true,
                ]
            ]
        ],
    ]
];
