const mix = require('laravel-mix');

// copy CRUD filters JS into packages
mix
	.copy('node_modules/@editorjs/editorjs/dist', 'public/packages/editorjs')
	.copy('node_modules/@editorjs/header/dist/bundle.js', 'public/packages/editorjs/tools/header.js')
	.copy('node_modules/@editorjs/list/dist/bundle.js', 'public/packages/editorjs/tools/list.js')
	.copy('node_modules/@editorjs/table/dist/bundle.js', 'public/packages/editorjs/tools/table.js')
	.copy('node_modules/editorjs-table/dist/bundle.js', 'public/packages/editorjs/tools/table2.js')
	.copy('node_modules/editorjs-hyperlink-browse/dist/bundle.js', 'public/packages/editorjs/tools/hyperlink.js')
    .copy('node_modules/@editorjs/embed/dist/bundle.js', 'public/packages/editorjs/tools/embed.js')
	.copy('node_modules/editorjs-image-browse/dist/bundle.js', 'public/packages/editorjs/tools/simple-image.js')
	.copy('node_modules/cropperjs/dist', 'public/packages/cropperjs/dist')
	.copy('node_modules/jsbarcode/dist', 'public/packages/jsbarcode/dist')
	.copy('node_modules/jsbarcode/dist/barcodes', 'public/packages/jsbarcode/dist/barcodes')
	.copy('node_modules/imask/dist/imask.min.js', 'public/packages/imask.js')
